require('dotenv').config()
const express = require('express')
const cloudinary = require('cloudinary')
const formData = require('express-form-data')
const cors = require('cors')
// const { CLIENT_ORIGIN } = require('./config')

const app = express()

cloudinary.config({ 
  cloudinary_url: process.env.CLOUDINARY_URL
})
  
// app.use(cors({ 
//   origin: CLIENT_ORIGIN 
// })) 

app.use(cors())


app.use(formData.parse())

app.get('/', (req, res) => {
    res.json({
        message: 'Welcome to the API'
    });
})

app.post('/image-upload', (req, res) => {
    const path = Object.values(req.files)[0].path
    cloudinary.v2.uploader.upload(path)
      .then(image => res.json([image]))
})

app.listen(process.env.PORT || 4000, () => console.log("Server running on port 4000"))